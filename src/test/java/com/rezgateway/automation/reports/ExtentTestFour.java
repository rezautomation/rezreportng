package com.rezgateway.automation.reports;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;


public class ExtentTestFour extends ExtentTestNGReportBuilder {
	
	@Test
	public void methodOneClassFour(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOneClassFour Test1");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");
			
	}
	@Test(dependsOnMethods="methodOneClassFour")
	public void methodTwoClassFour(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOneClassFour Test2");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "200");
		
	}

	@Test(dependsOnMethods="methodOneClassFour")
	public void methodThreeClassFour(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOneClassFour Test3");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");
		
	}

}
