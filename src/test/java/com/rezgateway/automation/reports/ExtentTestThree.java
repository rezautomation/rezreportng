package com.rezgateway.automation.reports;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;


public class ExtentTestThree extends ExtentTestNGReportBuilder {
	
	@Test
	public void methodOneClassThree(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOneClassThree Test1");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");
		
		
	}
	@Test(dependsOnMethods="methodOneClassThree")
	public void methodTwoClassThree(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOneClassThree Test2");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "200");
		
	}

	@Test(dependsOnMethods="methodOneClassThree")
	public void methodThreeClassThree(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOneClassThree Test3");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");
		
	}

}
