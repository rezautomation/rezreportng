package com.rezgateway.automation.reports;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.rezgateway.automation.utill.DriverFactory;

;

@SuppressWarnings("unused")
public class ExtentTest extends ExtentTestNGReportBuilder {

	@Test
	public void driver() throws Exception {
		DriverFactory.getInstance().initializeDriver();
		DriverFactory.getInstance().getDriver().get("http://192.168.0.65:8080/jenkins/login");
		ITestResult results = Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOne_Calculation Test1");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");
		//Assert.fail("No Results Error Code :");
	}

	@Test
	public void methodOne() {
		ITestResult results = Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOne_Calculation Test1");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");

	}

	@Test
	public void methodTwo() {
		ITestResult results = Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodTwo_Calculation Test2");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");

	}

	@Test
	public void methodThree() {
		ITestResult results = Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodThree_Calculation Test3");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");

	}

	@AfterTest
	public void tearDown(){
		DriverFactory.getInstance().getDriver().quit();
	}
	
}
