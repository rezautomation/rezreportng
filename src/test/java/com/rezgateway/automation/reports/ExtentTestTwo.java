package com.rezgateway.automation.reports;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;


public class ExtentTestTwo extends ExtentTestNGReportBuilder {
	
	@Test
	public void methodOneClassTwo(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodOneClassTwo_Calculation Test1");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");
	
	}
	@Test(dependsOnMethods="methodOneClassTwo")
	public void methodTwoClassTwo(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodTwoClassTwo_Calculation Test2");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "200");
		
	}

	@Test(dependsOnMethods="methodOneClassTwo")
	public void methodThreeClassTwo(){
		ITestResult results =  Reporter.getCurrentTestResult();
		results.setAttribute("TestName", "methodTwoClassTwo_Calculation Test3");
		results.setAttribute("Expected", "300");
		results.setAttribute("Actual", "300");
		
	}

}
