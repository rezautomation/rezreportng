package com.rezgateway.automation.reports;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;




import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentTestNGReportBuilderESB {

	private static ExtentReports extent;
	private static  ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
	private static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
	
	
	public String TEST_URL = "";

	
    @BeforeSuite(alwaysRun=true)
	public synchronized void beforeSuite() {
		extent = ExtentManager.createInstance("ExtentReport.html");
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("ExtentReport.html");
		extent.attachReporter(htmlReporter);
	}

	@BeforeClass(alwaysRun=true)
	public synchronized void beforeClass() {
		ExtentTest parent = extent.createTest(getClass().getName().substring(getClass().getName().lastIndexOf(".") + 1));
		parentTest.set(parent);
	}
	
	@AfterMethod(alwaysRun=true)
	public synchronized void afterMethod(ITestResult result) {

		//System.out.println(result.getStatus());
		ExtentTest child;
		String TestName;
		String Expected;
		String Actual;	
		String openHeader = "<pre style=background-color:lightslategray><h6>";
		String closeHeader = "</h6></pre>";
		
		try { TestName = result.getAttribute("TestName").toString(); } catch (Exception e) {TestName ="Test Name Unspecified"; }
		try { Expected = result.getAttribute("Expected").toString(); } catch (Exception e) {Expected ="Expected Result Unspecified";  }
		try { Actual = result.getAttribute("Actual").toString();  } catch (Exception e) {Actual ="Actual Result Unspecified";  }
			
		child = ((ExtentTest) parentTest.get()).createNode(TestName.replace("<H>", openHeader).replace("</H>", closeHeader));
		child.info("Expected :" + Expected);
		child.info("Actual   :"+ Actual );			
		
		test.set(child);
		if (result.getStatus() == ITestResult.FAILURE)
			((ExtentTest) test.get()).fail(result.getThrowable());
		else if (result.getStatus() == ITestResult.SKIP)
			((ExtentTest) test.get()).skip(result.getThrowable());
		else
			((ExtentTest) test.get()).pass("Test passed");

		extent.flush();
	}
}
